package com.eshop.watches.catalog.controller;

import java.io.*;
import java.util.List;
import java.util.Optional;

import com.eshop.watches.catalog.entity.Watch;
import com.eshop.watches.catalog.service.WatchesService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@CrossOrigin
@RestController
@RequestMapping("/watches")
public class WatchesController {


  Logger logger = LoggerFactory.getLogger(WatchesController.class);

  private final WatchesService service;

  public WatchesController(WatchesService service) {
    this.service = service;
  }

  @GetMapping
  public ResponseEntity<List<Watch>> getWatches() {

    File file = new File("counter.txt");
    try {
      boolean newFile = file.createNewFile();
      if (newFile){

        BufferedWriter writer = new BufferedWriter(new FileWriter("counter.txt"));
        writer.write("0");        
        writer.close();
      }
      
      BufferedReader reader = new BufferedReader(new FileReader(file));
      String line = reader.readLine();
      int value = Integer.parseInt(line);

      if (value == 4){
        logger.error("Unable to handle request. Memory Error");
        file.delete();
        reader.close();
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
      } else {
        BufferedWriter writer2 = new BufferedWriter(new FileWriter("counter.txt"));
        writer2.write(Integer.toString(value + 1));
        writer2.close();
      }
      reader.close();
      

    } catch (IOException e) {
      logger.error("Error reading file");
    }
    
    List<Watch> watches = service.getAllWatches();
    HttpStatus status = HttpStatus.OK;
    if (0 == watches.size()) status = HttpStatus.NO_CONTENT;
    return ResponseEntity.status(status).body(watches);
  }

  @GetMapping("/{id}")
  public ResponseEntity<Watch> getWatch(@PathVariable Long id) {
    Optional<Watch> watch = service.getWatch(id);

    HttpStatus status = HttpStatus.OK;
    if (!watch.isPresent()) status = HttpStatus.NO_CONTENT;
    return ResponseEntity.status(status).body(watch.orElse(new Watch()));
  }

  @GetMapping("/brands/{brandId}")
  public ResponseEntity<List<Watch>> getWatchesByBrandId(@PathVariable Long brandId) {
    List<Watch> watches = service.getWatchesByBrandId(brandId);
    HttpStatus status = HttpStatus.OK;
    if (0 == watches.size()) status = HttpStatus.NO_CONTENT;
    return ResponseEntity.status(status).body(watches);
  }

}
